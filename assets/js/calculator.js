$(() => {
  // Handle with Clicks on Numbers & Operators
  $(document).off('click', '[data-number]')
    .on('click', '[data-number]', (e) => {
      let value = $(e.currentTarget).text();
      let screen = $.trim($('#screen-result').text());

      if (screen === "ERRO") {
        $('#screen-result').text('');
        return false;
      }

      if ($.isNumeric(value) || (!$.isNumeric(value) && screenValidator(screen))) {
        $("#screen-result").text(screen + value);
        $('.operator').removeAttr('disabled');
      } else $('#screen-result').text('');
    })

  // Screen clear
  $(document).off('click', '[all-clear]')
    .on('click', '[all-clear]', (e) => {
      $('#screen-result').text('');
    })

  screenValidator = (screen) => {
    let operators = ["-", "+", "/", ".", "*", "mod"]
    var validation = true;
    for (let index = 0; index < operators.length; index++) {
      const element = operators[index];
      if (screen[screen.length - 1] === element) validation = false;
    }
    return validation;
  }

  // Handle with Result Click
  $(document).off('click', '[data-result]')
    .on('click', '[data-result]', (e) => {
      $("#operation").val($('#screen-result').text());
      let form = $('#form_request');
      $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: form.serialize(),
        dataType: 'json',
        success: (response) => {
          $('#screen-result').text(response.result);
          if (response.message.status.length) {
            let alertTemplate = '<div style="min-width:60%;" class="alert ' + response.message.status + '" role="alert">' + response.message.content + '</div>';
            $('.calculator').prepend(alertTemplate);
          }
        }
      });
    });

})
