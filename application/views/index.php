<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Calculator</title>
    <link rel="stylesheet" href="<?= base_url("assets/css/bootstrap.min.css");?>"/>
    <link rel="stylesheet" href="<?= base_url("assets/css/calculator.css");?>"/>
    <script rel="stylesheet" href="<?= base_url("assets/css/calculator.css");?>"></script>
    <script src="<?= base_url("assets/js/jquery-3.4.1.js");?>"></script>
    <script src="<?= base_url("assets/js/bootstrap.min.js");?>"></script>
    <script src="<?= base_url("assets/js/calculator.js");?>"></script>
</head>
    <body>
        <div class="container">
            <div class="calculator card">
            <span type="text" class="calculator-screen z-depth-1" id="screen-result" disabled> </span>
                <div class="calculator-keys">

                    <button type="button" data-number class="operator btn btn-info 4" value="+" disabled >+</button>
                    <button type="button" data-number class="operator btn btn-info" value="-" disabled>-</button>
                    <button type="button" data-number class="operator btn btn-info" value="*" disabled>*</button>
                    <button type="button" data-number class="operator btn btn-info" value="/" disabled>/</button>

                    <button type="button" data-number value="7" class="btn btn-light waves-effect">7</button>
                    <button type="button" data-number value="8" class="btn btn-light waves-effect">8</button>
                    <button type="button" data-number value="9" class="btn btn-light waves-effect">9</button>
                    <button type="button" data-number value="mod" class="operator btn btn-info" value="%" disabled>mod</button>

                    <button type="button" data-number value="4" class="btn btn-light waves-effect">4</button>
                    <button type="button" data-number value="5" class="btn btn-light waves-effect">5</button>
                    <button type="button" data-number value="6" class="btn btn-light waves-effect">6</button>

                    <button type="button" data-number value="1" class="btn btn-light waves-effect">1</button>
                    <button type="button" data-number value="2" class="btn btn-light waves-effect">2</button>
                    <button type="button" data-number value="3" class="btn btn-light waves-effect">3</button>

                    <button type="button" data-number value="0" class="btn btn-light waves-effect">0</button>
                    <button type="button" data-number class="decimal function btn btn-secondary" value=".">.</button>
                    <button type="button" all-clear class="btn btn-danger btn-sm" value="all-clear">AC</button>

                    <button type="button" data-result class="equal-sign operator btn btn-default" value="=" disabled>=</button>
                </div>
            </div>
        </div>
        <form id="form_request" action="<?= base_url("calculators") ?>" method="post" style="display:none;">
            <input id="operation" type="text" name="operation" value="" />
        </form>
    </body>
</html>