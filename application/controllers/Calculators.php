<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Calculators extends CI_Controller
{
	public function index()
	{
		$this->load->view('index');
	}

	public function calculate()
	{

		if ($this->hasValidParams()) {
			if (empty($this->input->post("operation"))) return;
			$ip = $this->input->ip_address();
			$operation = $this->input->post("operation");
			$calculator = new Calculator($ip, $operation);
			$calculator->setResult();
			$calculator->save();
			$calculator->setBonus();

			echo json_encode([
				"result" => $calculator->result,
				"message" => [
					"status" => $calculator->bonus ? "alert-success" : "",
					"content" => $calculator->bonus ? "Acertou no valor do bónus!" : "",
				],
			]);
		} else { // Invalid Operation
			echo json_encode([
				"result" => "ERRO",
				"message" => [
					"status" => "alert-danger",
					"content" => "Não é possível dividir por ZERO!",
				],
			]);
		}
	}
	//
	// Private methods
	//

	private function hasValidParams()
	{
		$operationArray = preg_split('//u', $this->input->post("operation"), null, PREG_SPLIT_NO_EMPTY);
		$isDividedByZero = false;
		foreach ($operationArray as $key => $value) {
			if ($value === "0" && $operationArray[$key - 1] === "/") {
				$isDividedByZero = true;
				break;
			}
		}
		if ( $isDividedByZero) return false;
		return true;
	}
}
