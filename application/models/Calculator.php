<?php 
class Calculator extends CI_Model {

	private $bonusNumber;

	public $ip;
	public $operation;
	public $result = 0;
	public $bonus = false;

	public function __construct($ip = null, $operation = null) {
		parent::__construct();
		$this->ip = $ip;
		$this->operation = $operation;
	}
	//
	// Methods
	//
	public function setResult() {
		$tempOperation = preg_replace('([^\\mod\\+\\-*\\/%\\^])', ' ', trim($this->operation));
		$tempOperation = explode(' ', trim($tempOperation));

		$this->operation = str_replace("mod", " ",$this->operation);
		$numbers = preg_replace('([^0-9\.,])', ' ', trim($this->operation));
		$numbers = explode(' ', $numbers);
		
		foreach ($numbers as $key => $val){
			if($key == 0 ){
				$this->result = $val;
				continue;
			}
			$this->makeAritmethicOperation($tempOperation[$key-1], $val);
		}
	}

	public function setBonus() {
		$this->bonusNumber = rand() / 10;
		if($this->bonusNumber == $this->result) $this->bonus = true;
		return $this->bonus;
	}
	
	public function save() {
		$data = array(
			'operation' => $this->operation,
			'result' => number_format($this->result,4,".",","),
			'bonus' => $this->bonus,
			'ip' => $this->ip
		);
		$this->db->insert('operations', $data);
		return ($this->db->affected_rows() != 1) ? false : true;
	}
	
	private function makeAritmethicOperation($operator, $value) {
		$operator =  $operator === "mod" ? "%" : $operator;
		switch($operator) {
			case '+':
				$this->result += floatval($value);
				break;
			case '-':
				$this->result -= floatval($value);
				break;
			case '/':
				$this->result /= floatval($value);
				break;
			case '*':
				$this->result *= floatval($value);
				break;
			case '%':
				$this->result %= floatval($value);
				break;
			}
	}

}